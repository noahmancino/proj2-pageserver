from flask import Flask, request, render_template, abort
import os
app = Flask(__name__)

@app.route("/")
def hello():
    return "UOCIS docker demo!"

@app.route("/<path:path>")
def serve_html_page(path):
    print(path)
    illegal_tokens = ['//', '~', '..']
    if any([x in path for x in illegal_tokens]): 
        abort(403)
    if not os.path.exists('./templates/' + path):
        abort(404)
    return render_template(path) #More error handling needs to check that path is 
                                 #needed to check if path is html, but I don't
@app.errorhandler(404)           #the tests are meant to catch this issue...
def page_not_found(error):
    return render_template('404.html'), 404

@app.errorhandler(403)
def page_not_allowed(error):
    return render_template('403.html'), 403 

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
