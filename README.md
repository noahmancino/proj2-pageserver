# A flask webapp demo for docker 

## What can it do?

The app can serve html files, and has built in error handling for 404s and 403s.

## Dependencies 

This app requires python, docker, and flask.

## How do I use it?

Enter the following into your terminal while in the web directory:

~~~
docker build -t container-name .
docker run -d -p 5000:5000 container-name
~~~

Then go to localhost:5000 on your machines browser.
